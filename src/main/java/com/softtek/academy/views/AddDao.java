package com.softtek.academy.views;

import com.softtek.academy.views.AddDao;
import com.softtek.academy.model.BeanModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class AddDao 
{
    public String checkInsert(BeanModel addBean)
    {
        String act=addBean.getAct();
        
        try
        {

        	Connection con = Conexion.getConnection(); 
            
            PreparedStatement pstmt=null; 
            
            pstmt=con.prepareStatement("insert into TO_DO_LIST (LIST) values (?)"); 
            pstmt.setString(1,act);
            pstmt.executeUpdate(); 
            
            pstmt.close();
            
            con.close(); 
            
            return "INSERT SUCCESS"; 
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return "FAIL INSERT"; 
        }
        
        
    }
    
}